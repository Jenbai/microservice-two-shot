from django.http import JsonResponse
import json
from django.views.decorators.http import require_http_methods

from common.json import ModelEncoder
from .models import Shoe, BinVO


# Create your views here.

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "image",
        "shoe_bin",
        "id",
    ]
    encoders = {"shoe_bin": BinVODetailEncoder()}

@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
            shoes = Shoe.objects.all()
            return JsonResponse(
                {"shoes": shoes},
                encoder=ShoeListEncoder,
                safe=False,
            )
    else:
        content = json.loads(request.body)
        try:
            shoe_bin = BinVO.objects.get(id=content["shoe_bin"])
            content["shoe_bin"] = shoe_bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_shoe(request, id):
    shoe = Shoe.objects.filter(id=id)
    shoe.delete()
    return JsonResponse(
        {
            "Status": f'Shoe id:{id} has been deleted'
        }
    )
