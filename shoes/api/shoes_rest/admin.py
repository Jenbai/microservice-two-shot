from django.contrib import admin
from .models import Shoe, BinVO

@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    pass

# Register your models here.
@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    pass
