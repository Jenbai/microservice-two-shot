# Generated by Django 4.0.3 on 2022-12-02 23:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_rename_hats_hat'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationvo',
            name='import_href',
            field=models.CharField(default=None, max_length=200, unique=True),
            preserve_default=False,
        ),
    ]
