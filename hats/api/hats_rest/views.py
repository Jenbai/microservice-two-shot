from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
from common.json import ModelEncoder
import json
# Create your views here.

#list hats, create a hat, delete a hat

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
    ]
class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]

    encoders={"location":LocationVODetailEncoder()}

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
       hats = Hat.objects.all()
       return JsonResponse(
        {"hats": hats},
        encoder = HatsListEncoder
       )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(id=content["location"])
            print(location)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_hat(request, id):
    hat = Hat.objects.filter(id=id)
    hat.delete()
    return JsonResponse({"Status": f'Hat id:{id} deleted'})
