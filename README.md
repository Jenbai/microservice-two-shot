# Wardrobify

Team:

* Jen Bailey - shoes microservice
* Diane Ancheril - Hats microservice

## Design

## Shoes microservice

I am going to create a Shoe model and a Bin virtual object in my shoe api Django app, I will use CharField for each field apart from the image field where I will use URLField, I will use a ForeignKey to the Bin virtual object for shoe_bin field. In the BinVO I will import href and closet_name.
In my view file I will create RESTful API requests to GET a list of shoes, POST a new shoe and DEL an existing shoe.

I will use a poller to poll for bin data from my wardrobe app to be accessed using the BinVO in my shoe app. I will import my BinVO model and create a get_bins function to update the BinVO object with the wardrobe bin data. I will create a poll function that polls for data and calls the get_bins function to update the BinVO.

I will then create a ShoeList class component in a ShoeList js file in our react service to get data from my shoe app and render it on the single app page. To this I will add a handleDelete event listener to be able to delete the selected shoe when using its delete button.

I will create a ShoeForm class component in a ShoeForm js file with onChange event listeners to create a new shoe. In my componentDidMount function I will make a request to the wardrobe app for the bin data and add this to the state, in my JSX I will loop through the bins using map to show each individual bin in the dropdown.

I our App.js I will import my ShoeList component and ShoeForm component and use BrowserRouter to route to the correct url and component. In my Nav.js I will add NavLinks to these components.

## Hats microservice

1. Create a Hat model and a LocationVO model
2. Register Hat model in admin.py
3. Configure Hat model in app.py
4. Register ‘hats_rest’ app in INSTALLED_APPS in settings.py
5. Create a view function called ‘api_list_hats’ to handle GET and POST requests to list all the hats
6. Register the view in urls.py file and include it in the project urls.py file
7. Define encoders to interpret view function data into JSON
8. For polling, import the LocationVO from models.py and copy data from the locations view in the wardrobe_api into the LocationVO
9. Define a poll function which polls for data using the get_locations()
