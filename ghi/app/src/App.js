import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm'
import HatForm from './HatForm'
import HatsList from './HatsList'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route path="" element={<ShoesList/>}/>
          </Route>
          <Route path="shoes">
            <Route path="new" element={<ShoeForm/>}/>
          </Route>

          <Route path="hats">
            <Route path="" element={<HatsList/>}/>
          </Route>
          <Route path="hats">
            <Route path="new" element={<HatForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
