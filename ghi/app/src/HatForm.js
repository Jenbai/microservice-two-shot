import React from 'react';
class HatForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            fabric:'',
            styleName: '',
            color: '',
            pictureUrl: '',
            locations: [],
        }

    }

    handleInputChange = (event) => {
        const name = event.target.name
        const value = event.target.value
        this.setState({[name]: value})
    }


    handleSubmit = async (event) => {
        event.preventDefault()
        const data = {...this.state}
        data.style_name = data.styleName; //matching it up to what is defined in the views
        data.picture_url = data.pictureUrl;
        console.log(data)
        delete data.styleName;
        delete data.pictureUrl;
        delete data.locations;

        const hatsUrl = `http://localhost:8090/api/hats/`
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch (hatsUrl, fetchConfig);
        if (response.ok){
            const newHat = await response.json()
            console.log(newHat)

            const cleared = { //name specified in input tag
                fabric: "",
                color: "",
                pictureUrl: "",
                styleName: "",
                location: "",
            }
            this.setState(cleared)

        }

    }

    async componentDidMount(){
        const response = await fetch('http://localhost:8100/api/locations/')
        if (response.ok) {
            const data = await response.json()
            this.setState({locations:data.locations})
        } else {
            console.log('Failed to fetch')
        }
    }

    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new Hat</h1>
                <form onSubmit={this.handleSubmit} id="create-shoe-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} placeholder="Fabric" type="text" name="fabric" id="fabric" value={this.state.fabric} className="form-control"/>
                    <label htmlFor="name">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} placeholder="Style name" type="text" name="styleName" id="style_name" value={this.state.styleName} className="form-control"/>
                    <label htmlFor="name">Style name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} placeholder="Color" type="text" name="color" id="color" value={this.state.color} className="form-control"/>
                    <label htmlFor="name">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleInputChange} placeholder="Picture url" type="text" name="pictureUrl" id="pictureUrl" value={this.state.pictureUrl} className="form-control"/>
                    <label htmlFor="name">Picture</label>
                    </div>
                  <div className="mb-3">
                    <select onChange={this.handleInputChange} name="location" id="locations" value={this.state.location} className="form-select">
                      <option>Choose a location</option>
                      {this.state.locations?.map(location => {
                            return(
                                <option key={location.id} value={location.id}>
                                    {location.closet_name}
                                </option>
                            )
                        })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
              </div>
              </div>
              )
    }

}

export default HatForm
