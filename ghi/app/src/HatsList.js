import React from 'react';

class HatsList extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            hats: []
        }

    }

    handleDelete = async (event) => {
        const id = event.target.id
        const response = await fetch(`http://localhost:8090/api/hats/${id}`, {method:"DELETE"})
        const data = await response.json()
        const r = await fetch('http://localhost:8090/api/hats/')
        const content = await r.json()
        console.log(content)
        this.setState({hats:content.hats})
    }

    async componentDidMount(){
        const response = await fetch('http://localhost:8090/api/hats/')
        if (response.ok) {
            const data = await response.json()
            console.log(data)
            this.setState({hats:data.hats})

        } else {
            console.log("Failed to fetch")
        }
    }

    render() {
        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.hats?.map(hat => {
                        return (
                            <tr key={hat.id}>
                                <td>{hat.fabric}</td>
                                <td>{hat.style_name}</td>
                                <td>{hat.color}</td>
                                <td><a href={hat.picture_url}><img width="100px" src={hat.picture_url}/></a></td>
                                <td>{hat.location}</td>
                                <td><button onClick={this.handleDelete} id={hat.id}>Delete</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        )
    }

}

export default HatsList
