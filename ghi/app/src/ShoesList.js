import React from "react"

class ShoesList extends React.Component {
    constructor(props){
        super(props)
        this.state = {shoes:[]}
    }

    handleDelete = async (event) => {
        const id = event.target.id
        const response = await fetch(`http://localhost:8080/api/shoes/${id}`, {method:"DELETE"})
        const reload = await fetch ('http://localhost:8080/api/shoes/')
        const content = await reload.json()
        this.setState({shoes:content.shoes})
    }

    async componentDidMount() {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            console.log(data.shoes)
            this.setState({shoes:data.shoes});
        }
        else{
            console.log("failed to fetch")
        }
    }
    render(){
    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Shoe Color</th>
                    <th>Shoe Image</th>
                    <th>Bin</th>
                </tr>
            </thead>
            <tbody>
            {this.state.shoes?.map(shoe => {
                return(
                    <tr key={shoe.manufacturer}>
                        <td>{shoe.manufacturer}</td>
                        <td>{shoe.model_name}</td>
                        <td>{shoe.color}</td>
                        <td> <img width="100px" src={shoe.image}/></td>
                        <td>{shoe.shoe_bin.closet_name}</td>
                        <td><button onClick={this.handleDelete} id={shoe.id}>Delete</button></td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
        }

}
export default ShoesList
