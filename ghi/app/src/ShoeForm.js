import React from 'react';

class ShoeForm extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            manufacturer: "",
            modelName: "",
            color: "",
            image: "",
            bins: [],
        };

    }


        handleSubmit = async (event)=>{
        event.preventDefault();
        const data = {...this.state};
        data.model_name = data.modelName;
        data.shoe_bin = data.shoeBin;
        delete data.shoeBin;
        delete data.modelName;
        delete data.bins
        delete data.conferences;

        const shoeUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch (shoeUrl, fetchConfig);
        if (response.ok){
            const newShoe = await response.json()
            console.log(newShoe)

            const cleared = {
                manufacturer: "",
                modelName: "",
                color: "",
                image: "",
                shoeBin: "",
            }
            this.setState(cleared)

        }

    }

    handleManufacturerChange = (event)=>{
        const value = event.target.value;
        this.setState({manufacturer:value})
    }

    handleModelNameChange = (event)=>{
        const value = event.target.value;
        this.setState({modelName:value})
    }

    handleColorChange = (event)=>{
        const value = event.target.value;
        this.setState({color:value})
    }

    handleImageChange = (event)=>{
        const value = event.target.value;
        this.setState({image:value})
    }

    handleBinChange = (event)=>{
        const value = event.target.value;
        this.setState({shoeBin:value})
    }



    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({bins:data.bins});
        }
    }
    render(){

        return(
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new presentation</h1>
                <form onSubmit={this.handleSubmit} id="create-shoe-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleManufacturerChange} placeholder="Manufacturer name" type="text" name="manufacturer" id="manufacturer" value={this.state.manufacturer} className="form-control"/>
                    <label htmlFor="name">Manufacturer name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleModelNameChange} placeholder="Model name" type="text" name="model_name" id="model_name" value={this.state.modelName} className="form-control"/>
                    <label htmlFor="name">Model name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleColorChange} placeholder="Color" type="text" name="color" id="color" value={this.state.color} className="form-control"/>
                    <label htmlFor="name">Color</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleImageChange} placeholder="Image url" type="text" name="image" id="image" value={this.state.image} className="form-control"/>
                    <label htmlFor="name">Image url</label>
                    </div>
                  <div className="mb-3">
                    <select onChange={this.handleBinChange} name="bins" id="bins" value={this.state.shoeBin} className="form-select">
                      <option  value="">Choose a bin</option>
                      {this.state.bins.map(shoeBin => {
                            return(
                                <option key={shoeBin.id} value={shoeBin.id}>
                                    {shoeBin.closet_name}
                                </option>
                            )
                        })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
              </div>
              </div>
              )
    }

}

export default ShoeForm
